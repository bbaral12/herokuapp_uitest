package testdata;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

public class DataProvider_Authenticate {
	@DataProvider(name = "FormAuthetication")
	public Object[][] getFormAuthenticationData(ITestContext c) {
		String validity = c.getCurrentXmlTest().getParameter("auth_validity");
		Object[][] groupArray = null;
		if (validity.equalsIgnoreCase("valid")) {
			groupArray = new Object[][]{{"tomsmith", "SuperSecretPassword!"}, {"tomsmith", "SuperSecretPassword!"}};
		} else if (validity.equalsIgnoreCase("invalid")) {
			groupArray = new Object[][]{{"tomsmith", "abcd"}, {"abcd", "SuperSecretPassword!"}, {"", ""},
					{"abcd", "efgh"}};
		}

		return groupArray;
	}

	@DataProvider(name = "BasicAuthetication")
	public Object[][] getBasicAuthenticationData(ITestContext c) {
		String validity = c.getCurrentXmlTest().getParameter("auth_validity");
		Object[][] groupArray = null;
		if (validity.equalsIgnoreCase("valid")) {
			groupArray = new Object[][]{{"admin", "admin"}, {"admin", "admin"}};
		} else if (validity.equalsIgnoreCase("invalid")) {
			groupArray = new Object[][]{{"admin", "abcd"}, {"abcd", "admin"}, {"", ""}, {"abcd", "efgh"}};
		}

		return groupArray;
	}
}
