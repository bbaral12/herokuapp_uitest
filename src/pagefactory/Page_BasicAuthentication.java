package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Page_BasicAuthentication {
	
	//test
	WebDriver driver;
	@FindBy(xpath = "//*[@id=\"content\"]/div/h3")
	WebElement titleText;

	public Page_BasicAuthentication(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void inputAuthparameters(String usrname, String pswd, String linkURL, String validity) {
		driver.get("http://" + usrname + ":" + pswd + "@" + "the-internet.herokuapp.com/basic_auth");
		if (validity.equalsIgnoreCase("valid")) {
			Assert.assertEquals(driver.getTitle(), "The Internet");
			checkPageTitle();
		} else {
			Assert.assertNotEquals(driver.getTitle(), "The Internet");
		}

		System.out.println("The basic authentication login with username:" + usrname + " and password:" + pswd + " is "
				+ validity + ".");
	}

	private void checkPageTitle() {
		Assert.assertEquals(titleText.getText(), "Basic Auth");
	}
}
