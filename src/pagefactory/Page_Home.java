package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Page_Home {
	WebDriver driver;
	@FindBy(xpath = "//*[@id=\"content\"]/h1")
	WebElement titleText;
	@FindBy(linkText = "A/B Testing")
	public WebElement aBTesting;
	@FindBy(linkText = "Add/Remove Elements")
	public WebElement addRemoveElements;
	@FindBy(linkText = "Basic Auth")
	public WebElement basicAuth;
	@FindBy(linkText = "Broken Images")
	public WebElement brokenImages;
	@FindBy(linkText = "Context Menu")
	public WebElement contextMenu;
	@FindBy(linkText = "Forgot Password")
	public WebElement forgotPassword;
	@FindBy(linkText = "Form Authentication")
	public WebElement formAuthentication;

	public Page_Home(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		checkPageTitle();
	}

	private void checkPageTitle() {
		Assert.assertEquals(titleText.getText(), "Welcome to the-internet");
	}
}
