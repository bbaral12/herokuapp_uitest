package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Page_ABTesting {
	WebDriver driver;
	@FindBy(xpath = "//*[@id=\"content\"]/div/h3")
	WebElement titleText;

	public Page_ABTesting(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		checkPageTitle();
	}

	private void checkPageTitle() {
		Assert.assertEquals(titleText.getText(), "Forgot Password");
	}
}
