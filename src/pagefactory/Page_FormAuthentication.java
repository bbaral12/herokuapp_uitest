package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Page_FormAuthentication {
	WebDriver driver;
	@FindBy(xpath = "//*[@id=\"content\"]/div/h2")
	WebElement titleText;
	@FindBy(id = "username")
	WebElement username;
	@FindBy(id = "password")
	WebElement password;
	@FindBy(xpath = "//*[@id=\"login\"]/button/i")
	WebElement loginBtn;
	@FindBy(id = "flash")
	WebElement msgconfirm;
	@FindBy(xpath = "//*[@id=\"flash\"]")
	WebElement msgconfirmtxt;
	@FindBy(xpath = "//*[@id=\"content\"]/div/a/i")
	WebElement Logoutbtn;

	public Page_FormAuthentication(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		checkPageTitle();
	}

	private void checkPageTitle() {
		Assert.assertEquals(this.titleText.getText(), "Login Page");
	}

	public void FormAuthetication_Login(String usrname, String pswd, String validity) {
		username.sendKeys(usrname);
		password.sendKeys(pswd);
		loginBtn.click();
		checkAuthentication(usrname, pswd, validity);
		if (validity.equalsIgnoreCase("valid")) {
			Logoutbtn.click();
		}

	}

	private void checkAuthentication(String usrname, String pswd, String validity) {
		if (validity.equalsIgnoreCase("valid")) {
			Assert.assertEquals(msgconfirm.getAttribute("class"), "flash success");
		} else {
			Assert.assertEquals(msgconfirm.getAttribute("class"), "flash error");
		}

		System.out.println("The login with username:" + usrname + " and password:" + pswd + " is " + validity + ".");
		System.out.println(msgconfirmtxt.getText());
	}
}
