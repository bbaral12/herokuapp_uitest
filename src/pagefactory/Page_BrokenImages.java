package pagefactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


public class Page_BrokenImages{
	WebDriver driver;
	ArrayList<Image> validImages = new ArrayList<Image>();
	ArrayList<Image> brokenImages = new ArrayList<Image>();
	@FindBy(xpath = "//*[@id=\"content\"]/div/h3")
	WebElement titleText;
	@FindBy(xpath = "//*[@id=\"content\"]/div/img")
	List<WebElement> imageElements;

	public Page_BrokenImages(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		checkPageTitle();
	}

	private void checkPageTitle() {
		Assert.assertEquals(titleText.getText(), "Broken Images");
	}

	public void getImageslist() {
		checkimages();
		Iterator<Image> var2 = validImages.iterator();

		Image i;
		while (var2.hasNext()) {
			i = var2.next();
			System.out.println("The image url " + i.imageURL + " is valid.");
		}

		var2 = brokenImages.iterator();

		while (var2.hasNext()) {
			i = var2.next();
			System.out.println("The image url " + i.imageURL + " is invalid with HTTP code:" + i.httpStausCode + ".");
		}

	}

	private void checkimages() {
		Iterator<WebElement> var2 = imageElements.iterator();

		while (var2.hasNext()) {
			WebElement imgElement = var2.next();
			String imgurl = imgElement.getAttribute("src");
			int httpcode = getHTTPResponseCode(imgurl);
			if (httpcode == 200) {
				validImages.add(new Image(imgurl, httpcode));
			} else {
				brokenImages.add(new Image(imgurl, httpcode));
			}
		}

	}

	public static int getHTTPResponseCode(String chkurl) {
		int respCode;
		try {
			HttpURLConnection huc = (HttpURLConnection) (new URL(chkurl)).openConnection();
			huc.setRequestMethod("HEAD");
			huc.connect();
			respCode = huc.getResponseCode();
		} catch (MalformedURLException var3) {
			var3.printStackTrace();
			respCode = 999;
		} catch (IOException var4) {
			var4.printStackTrace();
			respCode = 998;
		}

		return respCode;
	}
}

class Image {
	String imageURL;
	int httpStausCode;

	Image(String imageURL, int httpStausCode) {
		this.imageURL = imageURL;
		this.httpStausCode = httpStausCode;
	}
}