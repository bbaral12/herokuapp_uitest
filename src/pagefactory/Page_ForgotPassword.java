package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Page_ForgotPassword {
	WebDriver driver;
	@FindBy(xpath = "//*[@id=\"content\"]/div/h2")
	WebElement titleText;
	@FindBy(id = "email")
	WebElement emailaddbox;
	@FindBy(xpath = "//*[@id=\"form_submit\"]/i")
	WebElement retrivePasswordBtn;
	@FindBy(id="content")
	WebElement cofirmmsgbox;

	public Page_ForgotPassword(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		checkPageTitle();
	}

	private void checkPageTitle() {
		Assert.assertEquals(titleText.getText(), "Forgot Password");
	}

	public void submitEmailPswdRecovery(String emailadd) {
		emailaddbox.sendKeys(emailadd);
		retrivePasswordBtn.click();
		checkEmailSent();
	}

	private void checkEmailSent() {
		Assert.assertEquals(cofirmmsgbox.getText(), "Your e-mail's been sent!");
	}
}
