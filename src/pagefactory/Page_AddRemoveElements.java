package pagefactory;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Page_AddRemoveElements {
	WebDriver driver;
	@FindBy(xpath = "//*[@id=\"content\"]/h3")
	WebElement titleText;
	@FindBy(xpath = "//*[@id=\"content\"]/div/button")
	WebElement addElement;
	@FindBy(xpath = "//*[@id=\"elements\"]/button")
	List<WebElement> addedElement;
	@FindBy(id = "flash")
	WebElement msgconfirm;
	@FindBy(xpath = "//*[@id=\"flash\"]/text()")
	WebElement msgconfirmtxt;

	public Page_AddRemoveElements(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		checkPageTitle();
	}

	private void checkPageTitle() {
		Assert.assertEquals(titleText.getText(), "Add/Remove Elements");
	}

	public void addanElement() {
		int initialnoofelements = noofelements();
		addElement.click();
		Assert.assertEquals(addedElement.size(), initialnoofelements + 1);
	}

	public void removeanElement(int elementindex) {
		int initialnoofelements = noofelements();
		addedElement.get(elementindex).click();
		Assert.assertEquals(addedElement.size(), initialnoofelements - 1);
	}

	public int noofelements() {
		return addedElement == null ? 0 : addedElement.size();
	}
}
