package pagefactory;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Page_ContextMenu {
	WebDriver driver;
	@FindBy(xpath = "//*[@id=\"content\"]/div/h3")
	WebElement titleText;
	@FindBy(id = "hot-spot")
	public WebElement contextMenu;

	public Page_ContextMenu(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		checkPageTitle();
	}

	private void checkPageTitle() {
		Assert.assertEquals(titleText.getText(), "Context Menu");
	}

	public void verifyAlertTextandAccept() throws NoAlertPresentException {
		Alert alertbox = driver.switchTo().alert();
		Assert.assertEquals(alertbox.getText(), "You selected a context menu");
		alertbox.accept();
	}
}
