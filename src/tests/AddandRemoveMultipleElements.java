package tests;

import java.util.Random;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pagefactory.Page_AddRemoveElements;

public class AddandRemoveMultipleElements extends BaseClass {
	Page_AddRemoveElements objARE;

	@BeforeMethod
	public void gotoElementspage() {
		objHP.addRemoveElements.click();
		objARE = new Page_AddRemoveElements(driver);
	}

	@Test(priority = 0)
	public void addnrmvElements() {
		AddElements(5);
		RemoveElements(3);
	}

	private void AddElements(int elementstoadd) {
		for (int i = 0; i < elementstoadd; ++i) {
			objARE.addanElement();
		}
	}

	private void RemoveElements(int elementstoremove) {
		int totelements = objARE.noofelements();
		if (totelements < elementstoremove) {
			System.out.println("Not enough elements to remove, removing all existing elements");
			elementstoremove = totelements;
		}
		
		Random rand = new Random();
		for (int i = 0; i < elementstoremove; ++i) {
			objARE.removeanElement(rand.nextInt(totelements));
			--totelements;
		}

	}

	@AfterMethod
	public void gotoHomepage() {
		driver.get(testURL);
	}
}