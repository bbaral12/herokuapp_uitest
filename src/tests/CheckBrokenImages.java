package tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pagefactory.Page_BrokenImages;

public class CheckBrokenImages extends BaseClass {
	Page_BrokenImages objBI;

	@BeforeMethod
	public void gotoBrokenImagesPage() {
		objHP.brokenImages.click();
	}

	@Test
	public void AddElements() {
		objBI = new Page_BrokenImages(driver);
		objBI.getImageslist();
	}

	@AfterMethod
	public void gotoHomepage() {
		driver.get(testURL);
	}
}