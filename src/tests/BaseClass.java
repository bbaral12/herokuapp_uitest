package tests;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pagefactory.Page_Home;

public class BaseClass {
	WebDriver driver;
	Page_Home objHP;
	String testURL = "http://the-internet.herokuapp.com/";

	@BeforeClass
	public void setup() {
		System.setProperty("webdriver.chrome.silentOutput", "true");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		driver.get(testURL);
		objHP = new Page_Home(driver);

	}

	@AfterClass
	public void endTest() {
		driver.quit();
	}
}