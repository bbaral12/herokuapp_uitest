package tests;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pagefactory.Page_BasicAuthentication;
import pagefactory.Page_Home;
import testdata.DataProvider_Authenticate;

public class BasicAuthentication {
	WebDriver driver;
	Page_Home objHP;
	Page_BasicAuthentication objBA;
	String testURL = "http://the-internet.herokuapp.com";

	@BeforeMethod
	public void gotoFormAuthenticationPage() {
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
		driver.get(testURL);
	}

	@Test(dataProvider = "BasicAuthetication", dataProviderClass = DataProvider_Authenticate.class)
	public void BasicAuthenticate(String usrname, String pswd, ITestContext c) throws InterruptedException {
		objHP = new Page_Home(driver);
		String linkURL = objHP.basicAuth.getAttribute("href");
		String validity = c.getCurrentXmlTest().getParameter("auth_validity");
		objBA = new Page_BasicAuthentication(driver);
		objBA.inputAuthparameters(usrname, pswd, linkURL, validity);
	}

	@AfterMethod
	public void gobacktoHomepage(ITestContext c) {
		driver.quit();
	}
}