package tests;

import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pagefactory.Page_FormAuthentication;
import testdata.DataProvider_Authenticate;

public class FormAuthenticationLogin extends BaseClass {
	Page_FormAuthentication objFA;

	@BeforeMethod
	public void gotoFormAuthenticationPage() {
		objHP.formAuthentication.click();
	}

	@Test(dataProvider = "FormAuthetication", dataProviderClass = DataProvider_Authenticate.class)
	public void Authenticate(String usrname, String pswd, ITestContext c) throws InterruptedException {
		String validity = c.getCurrentXmlTest().getParameter("auth_validity");
		objFA = new Page_FormAuthentication(driver);
		objFA.FormAuthetication_Login(usrname, pswd, validity);
	}

	@AfterMethod
	public void gotoHomePage() {
		driver.get(testURL);
	}
}